from django.shortcuts import render, redirect
from rq.decorators import job
from memcache import RedisCache
from models import *
from forms import *
import urllib2
import re
import html5lib
import BeautifulSoup
import requests
import scrap
import memcache
import redis
Soup = BeautifulSoup.BeautifulSoup

conn = redis.StrictRedis(host='pub-redis-14642.eu-west-1-1.2.ec2.garantiadata.com', port=14642, db=0, password="zaq12wsx")
mc = RedisCache(conn)

def index(request):
    return render(request, 'index.html')
def onet(request):
    context = None

    onet_cache = mc.get('onet-cache')
    interia_cache = mc.get('interia-cache')
    wp_cache = mc.get('wp-cache')
    if onet_cache is None and interia_cache is None and wp_cache is None:
        Onett.objects.all().delete()

        onet_entries = scrap.onet("http://www.onet.pl")
        interia_entries = scrap.interia('http://www.interia.pl')
        wp_entries = scrap.wp('http://www.wp.pl')
        tabela_danych2 = []
        for o in onet_entries:
            if o is not None:
                tabela_danych2.append(o)
        for i in interia_entries:
            if i is not None:
                tabela_danych2.append(i)
        for w in wp_entries:
            if w is not None:
                tabela_danych2.append(w)
        for td2 in tabela_danych2:
            if td2 is not None:
                desc = Onett(description=td2)
                desc.save()
        onet_string = ''
        interia_string = ''
        wp_string = ''
        for i in onet_entries:
            if i is not None:
                onet_string += unicode(i) +'|'
        mc.set('onet-cache', onet_string, 60)
        for i in interia_entries:
            if i is not None:
                interia_string += unicode(i) + '|'
        mc.set('interia-cache', interia_string, 60)
        for i in wp_entries:
            if i is not None:
                wp_string += unicode(i) + '|'
        mc.set('wp-cache', wp_string, 60)
        context = {'onet': onet_entries, 'interia': interia_entries, 'wp': wp_entries}
    else:
        onet_cache_array = onet_cache.split('|')
        interia_cache_array = interia_cache.split('|')
        wp_cache_array = wp_cache.split('|')
        context = {'onet': onet_cache_array, 'interia': interia_cache_array, 'wp': wp_cache_array}
    return render(request, 'onet.html', context)

def interia(request):
    Interiaa.objects.all().delete()
    baza = scrap.scrap_webpage_interia('http://www.interia.pl','news-a')
    for j in baza:
        desc = Interiaa(description=j)
        desc.save()
    context = {'baza': baza}
    return render(request, 'interia.html', context)
def wp(request):
    Wpp.objects.all().delete()
    site_baza = scrap.scrap_webpage_wp('http://www.wp.pl')
    for j in site_baza:
        if j is not None:
            desc = Wpp(description=j)
            desc.save()
    context = {'baza': site_baza}
    return render(request, 'wp.html', context)
def wyszukane(request):
    zapytanie = request.session.get('lista')
    baza = Wyszukiwarkaa.objects.all()
    if baza.count() == 0:
        title = 'Brak newsow'
    else:
        title = 'Wyszukane newsy:'
    context = {'baza' : baza , 'title' : title , 'zapytanie' : zapytanie}
    return render(request, 'wyszukane.html', context)
def pobieranieNewsow():
    news = []
    onet_news = Onett.objects.all()
    interia_news = Interiaa.objects.all()
    wp_news = Wpp.objects.all()
    if onet_news is not None:
        for o in onet_news:
            news.append(o.description)
    if interia_news is not None:
        for o in interia_news:
            news.append(o.description)
    if wp_news is not None:
        for o in wp_news:
            news.append(o.description)
    return news
def szukaj(request):
    form = ComForm(request.POST)
    if form.is_valid():
        tak =0
        listaSlow = []
        Wyszukiwarkaa.objects.all().delete()
        slowo = form.cleaned_data.get('slowo')
        lista_wynikow_do_cache = []
        print(mc.get(str(slowo)))
        if mc.get(str(slowo)) is not None:
            naglowki_z_cache = mc.get(str(slowo))
            cache_array = naglowki_z_cache.split('|')
            for naglowek in cache_array:
                z = Wyszukiwarkaa(description=naglowek)
                z.save()
            return redirect('wyszukane')
        lista_slow_wersja_plus = slowo.split(' ')
        listaSlow = slowo.split()
        if ' ' in slowo:
            tak = 1
        else:
            tak = 0
        list = []
        list = pobieranieNewsow()
        for i in lista_slow_wersja_plus:
            print(i)
            if str(i[0]) == '+':
                tak = 2
        for i in lista_slow_wersja_plus:
            print(i)
            if str(i[0]) == '-':
                tak = 3
        if listaSlow[0][0] == '"':
            tak = 5
        g = 0
        if tak == 1 or tak == 0:
            for k in list:
                kk = []
                kk = re.sub('[^A-Za-z0-9]+', ' ', k).split()
                for j in kk:
                    for h in listaSlow:
                        if h.lower() == j.lower():
                            lista_wynikow_do_cache.append(k)
                            z = Wyszukiwarkaa(description = k)
                            z.save()
        if tak == 2:
            wymagane = []
            wybrane_naglowki = []
            count = 0
            for s in lista_slow_wersja_plus:
                if s[0] == '+':
                    slowo_do_dodania = s[1:]
                    wymagane.append(slowo_do_dodania)
                    lista_slow_wersja_plus.pop(count)
                count = count + 1
            for slowo_nie_wymagane in lista_slow_wersja_plus:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        wybrane_naglowki.append(naglowek)
            if len(lista_slow_wersja_plus) == 0:
                wybrane_naglowki = list
            for slowo_wymagane in wymagane:
                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        lista_wynikow_do_cache.append(naglowek)
                        z = Wyszukiwarkaa(description=naglowek)
                        z.save()
        if tak == 3:
            wymagane = []
            wybrane_naglowki = []
            count = 0
            for s in lista_slow_wersja_plus:
                if s[0] == '-':
                    slowo_do_dodania = s[1:]
                    wymagane.append(slowo_do_dodania)
                    lista_slow_wersja_plus.pop(count)
                count = count + 1
            for i in lista_slow_wersja_plus:
                print(i)
            for slowo_nie_wymagane in lista_slow_wersja_plus:
                for naglowek in list:
                    if slowo_nie_wymagane in naglowek:
                        print(naglowek)
                        wybrane_naglowki.append(naglowek)
            if len(lista_slow_wersja_plus) == 0:
                wybrane_naglowki = list
            for slowo_wymagane in wymagane:
                print('# '+slowo_wymagane)
                for naglowek in wybrane_naglowki:
                    if slowo_wymagane in naglowek:
                        print(naglowek)
                    else:
                        lista_wynikow_do_cache.append(naglowek)
                        z = Wyszukiwarkaa(description=naglowek)
                        z.save()
        if tak == 5:
            length = len(slowo)
            wyraz_arr = slowo[1:length-1]
            wyraz = ''.join([str(x) for x in wyraz_arr])
            print(wyraz)
            print(str(len(list)))
            for i in list:
                if wyraz in i:
                    lista_wynikow_do_cache.append(i)
                    z = Wyszukiwarkaa(description=i)
                    z.save()
        request.session['lista'] = slowo
        string = ''
        for i in lista_wynikow_do_cache:
            string += i + '|'
        mc.set(slowo, string, 30)
        return redirect('wyszukane')
    return render(request,'wyszukaj.html', locals())
