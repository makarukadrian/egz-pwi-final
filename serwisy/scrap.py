# -*- coding: utf-8 -*-
from datetime import datetime, date, time
import requests
import html5lib
from bs4 import BeautifulSoup


def onet(url):
    news_result = []

    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news_all = soup.find('div', id='newsBox1')
    separate_news = news_all.find_all('li')

    for news in separate_news[:3]:
        href = news.find('a', href=True)['href']
        title = news.find('span', {'class': 'newsTitle'}).string
        full_text = u''

        result = session.get(href)

        source = result.text
        soup = BeautifulSoup(source, 'html5lib')

        junk_text = soup.find('div', {'itemprop': 'articleBody'})

        if junk_text is not None:
            junk_text=junk_text.find_all('p')
            for text in junk_text:
                full_text += unicode(text.string)

        if title is None or full_text == u'':
            pass
        else:
            news_result.append(title + '     Opis: ' + full_text)

    return news_result
def interia(url):
    news_result = []
    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news_all = soup.find('ul', id='facts_news_small')
    separate_news = news_all.find_all('li')

    for news in separate_news[:3]:
        a = news.find('a', href=True)
        href = a['href']
        title = a.string
        full_text = u''

        result = session.get(href)

        source = result.text
        soup = BeautifulSoup(source, 'html5lib')

        junk_text = soup.find('div', {'itemprop': 'articleBody'})

        if junk_text is not None:
            junk_text=junk_text.find_all('p')
            for text in junk_text:
                full_text += unicode(text.string)

        if title is None or full_text == u'':
            pass
        else:
            news_result.append(title + '     Opis: ' + full_text)

    return news_result
def wp(url):
    news_result = []
    session = requests.session()
    result = session.get(url)

    source = result.text
    soup = BeautifulSoup(source, 'html5lib')

    news_all = soup.find('div', {'data-st-area': 'Wiadomosci'})
    separate_news = news_all.find_all('li')

    for news in separate_news[:3]:
        href = news.find('a', href=True)['href']
        title = news.find('h3').string
        full_text = u''

        result = session.get(href)

        source = result.text
        soup = BeautifulSoup(source, 'html5lib')

        junk_text = soup.find('article', id='intertext1')

        if junk_text is not None:
            for child in junk_text.descendants:
                full_text += unicode(child.string)

        if title is None or full_text == u'':
            pass
        else:
            news_result.append(title + '     Opis: ' + full_text)

    return news_result


def toString(element):
    if element is not None and element is not None:
        return ("".join([x if ord(x) < 128 else '?' for x in element])).strip()
