from django.conf.urls import patterns, include, url
from views import *



urlpatterns = patterns('',
    # Examples:
     url(r'^$', index, name='home'),
    url(r'^onet/', onet, name='onet'),
    url(r'^szukaj/', szukaj, name='wyszukaj'),
    url(r'^wyszukane/', wyszukane, name='wyszukane'),

)
